<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Crud extends CI_Model {

    function tampil_data(){//menampilkan data obat
        return $this->db->get('obat');
    }

    function tampil_data_supplier(){//menampilkan data supplier
        return $this->db->get('supplier');
    }

    function lihat_transaksi(){//menampilkan data obat
        return $this->db->get('transaksi');
    }

    public function get_product_obat($keyword){
			$this->db->select('*');
			$this->db->from('obat');
			$this->db->like('kode_obat',$keyword);
			$this->db->or_like('nama_obat',$keyword);
			$this->db->or_like('jenis_obat',$keyword);
			return $this->db->get()->result();
		}

    // public function get_product_obat_transaksi($keyword){
		// 	$this->db->select('*');
		// 	$this->db->from('obat');
		// 	$this->db->like('kode_obat',$keyword);
		// 	$this->db->or_like('nama_obat',$keyword);
		// 	return $this->db->get()->result();
		// }

    function input_transaksi($data,$table){
		    $this->db->insert($table,$data);
	  }

    function hapus_transaksi($where,$table){
    	$this->db->where($where);
    	$this->db->delete($table);
    }

    // function edit_data_transaksi($where,$table){
    //    return $this->db->get_where($table,$where);
    // }
    //
    // function update_data_transaksi($where,$data,$table){
    //     $this->db->where($where);
    //     $this->db->update($table,$data);
    // }

    public function get_product_supplier($keyword){
      $this->db->select('*');
      $this->db->from('supplier');
      $this->db->like('nama_supplier',$keyword);
      $this->db->or_like('alamat',$keyword);
      $this->db->or_like('barang_masuk',$keyword);
      return $this->db->get()->result();
    }
}

?>
