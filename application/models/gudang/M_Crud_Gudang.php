<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Crud_Gudang extends CI_Model {

    function tampil_obat(){//menampilkan data obat
        return $this->db->get('obat');
    }

    function tampil_supplier(){//menampilkan data supplier
        return $this->db->get('supplier');
    }

    function hapus_obat($where,$table){
    	$this->db->where($where);
    	$this->db->delete($table);
    }

    function hapus_supplier($where,$table){
    	$this->db->where($where);
    	$this->db->delete($table);
    }

    function input_obat($data,$table){
		    $this->db->insert($table,$data);
	  }

    function input_suplier($data,$table){
		    $this->db->insert($table,$data);
	  }

    function edit_data_obat($where,$table){
	     return $this->db->get_where($table,$where);
    }

    function update_data_obat($where,$data,$table){
    		$this->db->where($where);
    		$this->db->update($table,$data);
	  }

    function edit_data_supplier($where,$table){
	     return $this->db->get_where($table,$where);
    }

    function update_data_supplier($where,$data,$table){
    		$this->db->where($where);
    		$this->db->update($table,$data);
	  }

    public function get_product_obat($keyword){
			$this->db->select('*');
			$this->db->from('obat');
			$this->db->like('kode_obat',$keyword);
			$this->db->or_like('nama_obat',$keyword);
			$this->db->or_like('jenis_obat',$keyword);
			return $this->db->get()->result();
		}

    public function get_product_supplier($keyword){
      $this->db->select('*');
      $this->db->from('supplier');
      $this->db->like('nama_supplier',$keyword);
      $this->db->or_like('alamat',$keyword);
      $this->db->or_like('barang_masuk',$keyword);
      return $this->db->get()->result();
    }

}

?>
