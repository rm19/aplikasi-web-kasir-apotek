<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Crud_Manager extends CI_Model {

    function tampil_user(){//menampilkan data karyawan
      $where = "level != 'manager'";
        return $this->db->get_where('user',$where);
    }

    function hapus_user($where,$table){
    	$this->db->where($where);
    	$this->db->delete($table);
    }

    function input_user($data,$table){
		    $this->db->insert($table,$data);
	  }

    function edit_user($where,$table){
	     return $this->db->get_where($table,$where);
    }

    function update_user($where,$data,$table){
    		$this->db->where($where);
    		$this->db->update($table,$data);
	  }

    public function get_product_keyword($keyword){
      $where = "level != 'manager'";
			$this->db->select('*');
			$this->db->from('user');
			$this->db->where($where);
			$this->db->like('username',$keyword);
			$this->db->or_like('level',$keyword);
			return $this->db->get()->result();
		}

}

?>
