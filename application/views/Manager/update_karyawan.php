<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/css/sb-admin.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css');?>" rel="stylesheet">
    <title>Sistem Apotik | Dashboard</title>
</head>
<body>
<nav class="navbar navbar-expand navbar-dark bg-dark static-top">
      <a class="navbar-brand mr-1" href="<?php echo site_url('gudang_admin/c_gudang/index');?>">Sistem Kasir</a>
    <img style="width:40px; height:auto; margin-left:1560px;"src="<?php echo base_url('assets/image/user.png');?>"/>
    <a class="text-light" style="margin-left:15px;"> <?php echo $username; ?></a>
     <a href="<?php echo site_url('admin/c_admin/logout'); ?>"><img onclick = "if (! confirm('Are you sure want to logout?')) return false;" style="width:40px; height:auto; margin-left:20px;"src="<?php echo base_url('assets/image/logout.png');?>"/></>
    </nav>

   <div id="wrapper" style="background-color:white;">
      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('manager/C_crud_manager/index');?>">
            <i class="fas fa-fw fa-table"></i>
            <span>Data Karyawan</span></a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo site_url('manager/c_manager/form_register');?>">
            <i class="fas fa-fw fa-box"></i>
            <span style="">Register Karyawan</span></a>
        </li>
        </ul>
     <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo site_url('manager/c_manager/form_register');?> ">Register Karyawan</a>
            </li>
          </ol>

          <!-- Page Content -->
           <div class="card mb-3">
            <div class="card-body">
              <div class="col-md-9">
              <div class="panel panel-default">
               <div class="panel-body">
                 <?php foreach($user as $u){ ?>
                <form class="form-horizontal"  action="<?php echo site_url('manager/c_crud_manager/update_karyawan');?>" method="post">
                    <div class="form-group">
                      <label class="control-label col-md-3" for="kode_barang">Username :</label>
                      <div class="col-md-15">
                        <input type="hidden" class="form-control reset" name="uid" value="<?php echo $u->uid ?>">
                        <input type="text" class="form-control reset" name="username" value="<?php echo $u->username ?>">
                      </div>
                    </div> <!--tutup-->

                        <div class="form-group">
                      <label class="control-label col-md-3" for="kode_barang">Password :</label>
                      <div class="col-md-15">
                        <input type="text" class="form-control reset" name="password" value="<?php echo $u->password ?>">
                      </div>
                    </div> <!--tutup-->

                    <div class="form-group">
                      <label class="control-label col-md-3" for="kode_barang">Role :</label>
                      <div class="col-md-15">
                        <select class="" name="level">
                          <option value="admin">Admin</option>
                          <option value="gudang_admin">Gudang Admin</option>
                          <option value="manager">Manager</option>
                        </select>
                      </div>
                    </div> <!--tutup-->


                        <input class="btn btn-primary btn-lg" type="submit" name="Submit" value="tambah">
                    </form>
                  <?php } ?>
                    </div>
                  </div>
                </div>
            </div><!-- end col-md-9 -->

            </div>
          </div>
    <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer bg-dark text-light">
          <div class="container my-auto" style="width:100%;">
            <div class="copyright text-center my-auto">
              <span>Copyright © Kasir 2018</span>
            </div>
          </div>
        </footer>

      </div>
    </div>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>
</body>
</html>
