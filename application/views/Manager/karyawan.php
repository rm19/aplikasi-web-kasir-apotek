<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/css/sb-admin.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css');?>" rel="stylesheet">
    <title>Sistem Apotik | Dashboard</title>
</head>
<body>
<nav class="navbar navbar-expand navbar-dark bg-dark static-top">
      <a class="navbar-brand mr-auto" href="<?php echo site_url('gudang_admin/c_gudang/index');?>">Sistem Kasir</a>
    <img style="width:40px; height:auto; "src="<?php echo base_url('assets/image/user.png');?>"/>
    <a class="text-light"> <?php echo $username; ?></a>
     <a href="<?php echo site_url('admin/c_admin/logout'); ?>"><img onclick = "if (! confirm('Are you sure want to logout?')) return false;" style="width:40px; height:auto; "src="<?php echo base_url('assets/image/logout.png');?>"/></>
    </nav>

   <div id="wrapper" style="background-color:white;">
      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo site_url('manager/C_crud_manager/index');?>">
            <i class="fas fa-fw fa-table"></i>
            <span>Data Karyawan</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('manager/c_manager/form_register');?>">
            <i class="fas fa-fw fa-box"></i>
            <span style="">Register Karyawan</span></a>
        </li>
      </ul>
     <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo site_url('manager/c_crud_manager/index');?>">Data Karyawan</a>
            </li>
          </ol>

          <!-- Page Content -->
          <div class="card mb-3">
              <div class="card-header">
                <i class="fas fa-table"></i>
                Data Karyawan
              </div>
            <div class="card-body">
              <?php echo form_open('manager/c_crud_manager/search') ?>
            		<input type="text" name="keyword" placeholder="search">
            		<input type="submit" name="search_submit" value="Cari">
            	<?php echo form_close() ?>
              <br/>
              <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Username</th>
                <th>Password</th>
                <th>Role</th>
                <th>Pilihan</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
            <?php
                foreach($user as $u){
            ?>
                <tr>
                  <td><?php echo $u->username ?></td>
                  <td><?php echo $u->password ?></td>
                  <td><?php echo $u->level ?></td>
                  <td>
                    <span class="badge badge-success">
                      <?php echo anchor('manager/c_crud_manager/edit_karyawan/'.$u->uid,'Edit'); ?>
                    </span>
                    <span class="badge badge-danger">
                      <?php echo anchor('manager/c_crud_manager/hapus_karyawan/'.$u->uid,'Hapus'); ?>
                    </span>
                  </td>
                </tr>
                <?php } ?>
          </table>
        </div>

            </div>
          </div>
    <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer bg-dark text-light">
          <div class="container my-auto" style="width:100%;">
            <div class="copyright text-center my-auto">
              <span>Copyright © Kasir 2018</span>
            </div>
          </div>
        </footer>

      </div>
    </div>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>
</body>
</html>
