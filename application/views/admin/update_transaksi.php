<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/css/sb-admin.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css');?>" rel="stylesheet">
    <title>Sistem Apotik | Kasir</title>
</head>
<body>
<nav class="navbar navbar-expand navbar-dark bg-dark static-top">
      <a class="navbar-brand mr-1" href="<?php echo site_url('admin/c_admin/index');?>">Sistem Kasir</a>
	  <img style="width:40px; height:auto; margin-left:1470px;"src="<?php echo base_url('assets/image/user.png');?>"/>
	  <a class="text-light" style="margin-left:15px;"> <?php echo $username; ?></a>
	   <a href="<?php echo site_url('admin/c_admin/logout'); ?>"><img onclick = "if (! confirm('Are you sure want to logout?')) return false;" style="width:40px; height:auto; margin-left:20px;"src="<?php echo base_url('assets/image/logout.png');?>"/></>
    </nav>

	 <div id="wrapper" style="background-color:white;">
      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo site_url('admin/c_admin/index');?>">
            <i class="fas fa-fw fa-money-bill"></i>
            <span>Kasir</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('admin/c_crud_kasir/tampil_obat');?>">
            <i class="fas fa-fw fa-database"></i>
            <span style="">Data Obat</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('admin/c_crud_kasir/tampil_supplier');?>">
            <i class="fas fa-fw fa-table"></i>
            <span>Data Supplier</span></a>
        </li>
		 <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('admin/c_crud_kasir/tampil_transaksi');?>">
            <i class="fas fa-fw fa-newspaper"></i>
            <span>Transaksi</span></a>
        </li>
      </ul>
	   <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a>Kasir</a>
            </li>
          </ol>
          <div class="card mb-3">
            <div class="card-body">
              <div class="col-md-9">
          		<div class="panel panel-default">
          		 <div class="panel-body">
                   <?php foreach($transaksi as $u){ ?>
          		 	<form class="form-horizontal" id="form_transaksi"  action="<?php echo site_url('admin/c_crud_kasir/update_transaksi');?>" method="post">
          	      	<div class="col-md-8">
          			    <div id="barang">
                      <div class="form-group">
                        <label class="control-label col-md-3" for="nama_barang">Tanggal :</label>
                        <div class="col-md-8">
                          <input type="hidden" class="form-control reset" name="kode_transaksi"  value="<?php echo $u->kode_transaksi?>">
                          <input type="date" class="form-control reset" name="tgl"  value="<?php echo $u->tanggal ?>">
                        </div>
                      </div>
            				    <div class="form-group">
            				      <label class="control-label col-md-3" for="nama_barang">Kode Obat :</label>
            				      <div class="col-md-8">
            				        <input type="text" class="form-control reset" name="kode_obat"  value="<?php echo $u->kode_obat ?>">
            				      </div>
            				    </div>
          				    <div class="form-group">
          				      <label class="control-label col-md-3" for="nama_barang">Nama Obat :</label>
          				      <div class="col-md-8">
          				        <input type="text" class="form-control reset" name="nama_obat"  value="<?php echo $u->nama_obat ?>">
          				      </div>
          				    </div>
          				    <div class="form-group">
          				      <label class="control-label col-md-3"
          				      	for="harga_barang">Harga (Rp) :</label>
          				      <div class="col-md-8">
          				        <input type="text" class="form-control reset" name="harga_jual"  value="<?php echo $u->total_penjualan ?>">
          				      </div>
          				    </div>
          				    <div class="form-group">
          				      <label class="control-label col-md-3"
          				      	for="qty">Quantity :</label>
          				      <div class="col-md-4">
          				        <input type="number"  id="qty" min="0" name="qty" placeholder="Isi qty..."  value="<?php echo $u->total_barang ?>">
          				      </div>
          				    </div>
                     <input type="submit" name="simpan" value="simpan">
          			    </div><!-- end id barang -->
          			      <!-- </div>
          			    </div> --><!-- end panel-->
          	      	</div><!-- end col-md-8 -->
          	      	</div><!-- end col-md-4 -->
          	      	</form>
                    <?php } ?>
          	      </div>
          	    </div>
          	</div><!-- end col-md-9 -->

            </div>
          </div>
        </div>

          <!-- Page Content -->


        </div>
		<!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer bg-dark text-light">
          <div class="container my-auto" style="width:100%;">
            <div class="copyright text-center my-auto">
              <span>Copyright © Kasir 2018</span>
            </div>
          </div>
        </footer>

      </div>
    </div>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>
</body>
</html>
