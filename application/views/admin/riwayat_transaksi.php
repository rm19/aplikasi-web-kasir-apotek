<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/css/sb-admin.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css');?>" rel="stylesheet">
    <title>Sistem Apotik | Kasir</title>
</head>
<body>
<nav class="navbar navbar-expand navbar-dark bg-dark static-top">
      <a class="navbar-brand mr-1" href="<?php echo site_url('admin/c_admin/index');?>">Sistem Kasir</a>
	  <img style="width:40px; height:auto; margin-left:1560px;"src="<?php echo base_url('assets/image/user.png');?>"/>
	  <a class="text-light" style="margin-left:15px;"> <?php echo $username; ?></a>
    <a href="<?php echo site_url('admin/c_admin/logout'); ?>"><img onclick = "if (! confirm('Are you sure want to logout?')) return false;" style="width:40px; height:auto; margin-left:20px;"src="<?php echo base_url('assets/image/logout.png');?>"/></>
   </nav>

	 <div id="wrapper" style="background-color:white;">
      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('admin/c_admin/index');?>">
            <i class="fas fa-fw fa-money-bill"></i>
            <span>Kasir</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('admin/c_crud_kasir/tampil_obat');?>">
            <i class="fas fa-fw fa-database"></i>
            <span style="">Data Obat</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('admin/c_crud_kasir/tampil_supplier');?>">
            <i class="fas fa-fw fa-table"></i>
            <span>Data Supplier</span></a>
        </li>
		 <li class="nav-item active">
          <a class="nav-link" href="<?php echo site_url('admin/c_crud_kasir/tampil_transaksi');?>">
            <i class="fas fa-fw fa-newspaper"></i>
            <span>Transaksi</span></a>
        </li>
      </ul>
	   <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a>Data Obat</a>
            </li>
          </ol>
          <div class="card mb-3">
              <div class="card-header">
                <i class="fas fa-table"></i>
                Data Transaksi
              </div>
            <div class="card-body">
              <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Kode Obat</th>
                <th>Nama Obat</th>
                <th>Tanggal Transaksi</th>
                <th>Jumlah</th>
                <th>Total Belanja</th>
                <th>Pilihan</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
            <?php
                foreach($transaksi as $u){
            ?>
                <tr>
                  <td><?php echo $u->kode_obat ?></td>
                  <td><?php echo $u->nama_obat ?></td>
                  <td><?php echo $u->tanggal ?></td>
                  <td><?php echo $u->total_barang ?></td>
                  <td><?php echo $u->total_penjualan ?></td>
                  <td>
                    <?php echo anchor('admin/c_crud_kasir/hapus_transaksi/'.$u->kode_transaksi,'Hapus'); ?>
                  </td>
                </tr>
                <?php } ?>
          </table>
        </div>

            </div>
          </div>
        </div>

          <!-- Page Content -->


        </div>
		<!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer bg-dark text-light">
          <div class="container my-auto" style="width:100%;">
            <div class="copyright text-center my-auto">
              <span>Copyright © Kasir 2018</span>
            </div>
          </div>
        </footer>

      </div>
    </div>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>
</body>
</html>
