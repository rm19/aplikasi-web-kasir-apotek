<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/css/sb-admin.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css');?>" rel="stylesheet">
    <title>Sistem Apotik | Dashboard</title>
</head>
<body>
<nav class="navbar navbar-expand navbar-dark bg-dark static-top">
      <a class="navbar-brand mr-1" href="<?php echo site_url('gudang_admin/c_gudang/index');?>">Sistem Kasir</a>
    <img style="width:40px; height:auto; margin-left:1560px;"src="<?php echo base_url('assets/image/user.png');?>"/>
    <a class="text-light" style="margin-left:15px;"> <?php echo $username; ?></a>
     <a href="<?php echo site_url('admin/c_admin/logout'); ?>"><img onclick = "if (! confirm('Are you sure want to logout?')) return false;" style="width:40px; height:auto; margin-left:20px;"src="<?php echo base_url('assets/image/logout.png');?>"/></>
    </nav>

   <div id="wrapper" style="background-color:white;">
      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('gudang_admin/c_gudang/index');?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dasboard</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('gudang_admin/c_gudang/register_obat');?>">
            <i class="fas fa-fw fa-box"></i>
            <span style="">Register Obat</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('gudang_admin/c_crud_gudang/tampil_obat');?>">
            <i class="fas fa-fw fa-table"></i>
            <span>Data Obat</span></a>
        </li>
    <li class="nav-item active">
          <a class="nav-link" href="<?php echo site_url('gudang_admin/c_gudang/register_suplier');?>">
            <i class="fas fa-fw fa-user"></i>
            <span>Register Supplier</span></a>
        </li>
     <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('gudang_admin/c_crud_gudang/tampil_suplier');?>">
            <i class="fas fa-fw fa-briefcase"></i>
            <span>Data Supplier</span></a>
        </li>
      </ul>
     <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo site_url('gudang_admin/c_gudang/register_obat');?> ">Register Supplier</a>
            </li>
          </ol>

          <!-- Page Content -->
           <div class="card mb-3">
            <div class="card-body">
              <div class="col-md-9">
              <div class="panel panel-default">
               <div class="panel-body">
                 <?php foreach($supplier as $u){ ?>
                <form class="form-horizontal"  action="<?php echo site_url('gudang_admin/c_crud_gudang/update_supplier');?>" method="post">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="kode_barang">Nama Supplier :</label>
                    <div class="col-md-15">
                      <input type="hidden" class="form-control reset" name="id_supplier" value="<?php echo $u->id_supplier ?>">
                      <input type="text" class="form-control reset" name="nama_supplier" value="<?php echo $u->nama_supplier ?>">
                    </div>
                  </div> <!--tutup-->

                  <div class="form-group">
                    <label class="control-label col-md-3" for="kode_barang">Alamat Supplier :</label>
                    <div class="col-md-15">
                      <input type="text" class="form-control reset" name="alamat" value="<?php echo $u->alamat ?>">
                    </div>
                  </div> <!--tutup-->

                  <div class="form-group">
                    <label class="control-label col-md-3" for="kode_barang">Kontak Supplier :</label>
                    <div class="col-md-15">
                      <input type="text" class="form-control reset" name="kontak" value="<?php echo $u->kontak?>">
                    </div>
                  </div> <!--tutup-->

                  <div class="form-group">
                    <label class="control-label col-md-3" for="kode_barang">Barang Masuk :</label>
                    <div class="col-md-15">
                      <input type="text" class="form-control reset" name="barang_masuk" value="<?php echo $u->barang_masuk ?>">
                    </div>
                  </div> <!--tutup-->

                  <div class="form-group">
                    <label class="control-label col-md-3" for="kode_barang">Harga Pembelian :</label>
                    <div class="col-md-15">
                      <input type="text" class="form-control reset" name="harga_beli" value="<?php echo $u->harga_beli ?>">
                    </div>
                  </div> <!--tutup-->
                        <input class="btn btn-primary btn-lg" type="submit" value="Simpan">
                    </form>
                    <?php } ?>

                    </div>
                  </div>
                </div>
            </div><!-- end col-md-9 -->

            </div>
          </div>
    <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer bg-dark text-light">
          <div class="container my-auto" style="width:100%;">
            <div class="copyright text-center my-auto">
              <span>Copyright © Kasir 2018</span>
            </div>
          </div>
        </footer>

      </div>
    </div>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>
</body>
</html>
