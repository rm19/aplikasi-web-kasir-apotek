<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/css/sb-admin.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css');?>" rel="stylesheet">
    <title>Sistem Apotik | Dashboard</title>
</head>
<body>
<nav class="navbar navbar-expand navbar-dark bg-dark static-top">
      <a class="navbar-brand mr-auto" href="<?php echo site_url('gudang_admin/c_gudang/index');?>">Sistem Kasir</a>
	  <img style="width:40px; height:auto; "src="<?php echo base_url('assets/image/user.png');?>"/>
	  <a class="text-light" "> <?php echo $username; ?></a>
	   <a href="<?php echo site_url('admin/c_admin/logout'); ?>"><img onclick = "if (! confirm('Are you sure want to logout?')) return false;" style="width:40px; height:auto; "src="<?php echo base_url('assets/image/logout.png');?>"/></>
    </nav>

	 <div id="wrapper" style="background-color:white;">
      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo site_url('gudang_admin/c_gudang/index');?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dasboard</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('gudang_admin/c_gudang/register_obat');?>">
            <i class="fas fa-fw fa-box"></i>
            <span style="">Register Obat</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('gudang_admin/c_crud_gudang/tampil_obat');?>">
            <i class="fas fa-fw fa-table"></i>
            <span>Data Obat</span></a>
        </li>
    <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('gudang_admin/c_gudang/register_suplier');?>">
            <i class="fas fa-fw fa-user"></i>
            <span>Register Supplier</span></a>
        </li>
     <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('gudang_admin/c_crud_gudang/tampil_suplier');?>">
            <i class="fas fa-fw fa-briefcase"></i>
            <span>Data Supplier</span></a>
        </li>
      </ul>
	   <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.html">Dashboard</a>
            </li>
          </ol>

          <!-- Page Content -->
           <div class="row">
            <div class="col-xl-6 col-sm-6 mb-1">
              <div class="card text-white bg-dark o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-mail-bulk"></i>
                  </div>
                  <div class="mr-5">Register Obat</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="<?php echo site_url('gudang_admin/c_gudang/register_obat');?>">
                  <span class="float-left"></span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-xl-6 col-sm-6 mb-1">
              <div class="card text-white bg-info o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-table"></i>
                  </div>
                  <div class="mr-5">Data Obat</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="<?php echo site_url('gudang_admin/c_crud_gudang/tampil_obat');?>">
                  <span class="float-left"></span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
			</div>
			</br>
			<div class="row">
            <div class="col-xl-6 col-sm-6 mb-1">
              <div class="card text-white bg-danger o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-chart-bar"></i>
                  </div>
                  <div class="mr-5">Register Supplier</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="<?php echo site_url('gudang_admin/c_gudang/register_suplier');?>">
                  <span class="float-left"></span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-xl-6 col-sm-6 mb-1">
              <div class="card text-white bg-secondary o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-user"></i>
                  </div>
                  <div class="mr-5">Data Supplier</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="<?php echo site_url('gudang_admin/c_crud_gudang/tampil_suplier');?>">
                  <span class="float-left"></span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
          </div>

        </div>
		<!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer bg-dark text-light">
          <div class="container my-auto" style="width:100%;">
            <div class="copyright text-center my-auto">
              <span>Copyright © Kasir 2018</span>
            </div>
          </div>
        </footer>

      </div>
    </div>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>
</body>
</html>
