<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">
    <title>Sistem Kasir | Login</title>
</head>
<body>
  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
       <a class="navbar-brand mr-1" href="<?php echo site_url('auth/index');?>" style="margin-left:26px; font-size:28px;">Sistem Kasir</a>
       <ul class="navbar-nav ml-auto ml-md-0">
         <li class="nav-item dropdown no-arrow mx-1">
         </li>
       </ul>
   </nav>
<?php echo form_open("auth/cek_login"); ?>
  <div class="container">
  <h1 class="text-center text-light pt-1 mt-5">Login</h1>
  <div class="row" id="pwd-container">
    <div class="col-md-4"></div>

    <div class="col-md-4">
      <section class="login-form">
        <form method="post" action="" role="login">
          <img src="" class="img-responsive" alt="" />
          <input type="text" name="username" placeholder="username" required class="form-control input-lg"/>
          <input type="password" class="form-control input-lg" name="password" placeholder="Password" required="" />
          <div class="pwstrength_viewport_progress"></div>
          <button type="submit" name="go" class="btn btn-lg btn-primary btn-block" >Sign in</button>
        </form>
      </section>
      </div>

      <div class="col-md-4"></div>


  </div>

</div>
		<?php echo form_close(); ?>

</body>
</html>
