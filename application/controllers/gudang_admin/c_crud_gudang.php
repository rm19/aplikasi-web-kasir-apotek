<?php
class C_crud_gudang extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="") {
			redirect('auth');
		}
		$this->load->helper('text');
		$this->load->model('gudang/M_Crud_Gudang');
	}

	public function tampil_obat() {
		$data['username'] = $this->session->userdata('username');
		$data['obat'] = $this->M_Crud_Gudang->tampil_obat()->result();
		$this->load->view('gudang_admin/gudang_obat', $data);
	}

	public function tampil_suplier() {
		$data['username'] = $this->session->userdata('username');
		$data['supplier'] = $this->M_Crud_Gudang->tampil_supplier()->result();
		$this->load->view('gudang_admin/gudang_supplier', $data);
	}

	public function hapus_obat($id_obat){
		$where = array('id_obat' => $id_obat);
		$this->M_Crud_Gudang->hapus_obat($where,'obat');
		redirect('gudang_admin/c_crud_gudang/tampil_obat');
	}

	public function hapus_supplier($id_supplier){
		$where = array('id_supplier' => $id_supplier);
		$this->M_Crud_Gudang->hapus_supplier($where,'supplier');
		redirect('gudang_admin/c_crud_gudang/tampil_suplier');
	}

	public function tambah_obat(){
		$kode_obat = $this->input->post('kode_obat');
		$nama_obat = $this->input->post('nama_obat');
		$jenis_obat = $this->input->post('jenis_obat');
		$satuan = $this->input->post('satuan');
		$stok_obat = $this->input->post('stok_obat');
		$harga_beli = $this->input->post('harga_beli');
		$harga_jual = $this->input->post('harga_jual');

		$data = array(
			'kode_obat' => $kode_obat,
			'nama_obat' => $nama_obat,
			'jenis_obat' => $jenis_obat,
			'satuan' => $satuan,
			'stok_obat' => $stok_obat,
			'harga_beli' => $harga_beli,
			'harga_jual' => $harga_jual
			);

		$this->M_Crud_Gudang->input_obat($data,'obat');
		redirect('gudang_admin/c_crud_gudang/tampil_obat');
	}

	public function tambah_supplier(){
		$nama_supplier = $this->input->post('nama_supplier');
		$alamat = $this->input->post('alamat');
		$kontak = $this->input->post('kontak');
		$barang_masuk = $this->input->post('barang_masuk');
		$harga_beli = $this->input->post('harga_beli');

		$data = array(
			'nama_supplier' => $nama_supplier,
			'alamat' => $alamat,
			'kontak' => $kontak,
			'barang_masuk' => $barang_masuk,
			'harga_beli' => $harga_beli
			);

		$this->M_Crud_Gudang->input_suplier($data,'supplier');
		redirect('gudang_admin/c_crud_gudang/tampil_suplier');
	}

	public function edit_obat($id_obat){
		$where = array('id_obat' => $id_obat);
		$data['username'] = $this->session->userdata('username');
		$data['obat'] = $this->M_Crud_Gudang->edit_data_obat($where,'obat')->result();
		$this->load->view('gudang_admin/update_obat',$data);
	}

	public function update_obat(){
		$id_obat = $this->input->post('id_obat');
		$kode_obat = $this->input->post('kode_obat');
		$nama_obat = $this->input->post('nama_obat');
		$jenis_obat = $this->input->post('jenis_obat');
		$satuan = $this->input->post('satuan');
		$stok_obat = $this->input->post('stok_obat');
		$harga_beli = $this->input->post('harga_beli');
		$harga_jual = $this->input->post('harga_jual');

		$data = array(
			'kode_obat' => $kode_obat,
			'nama_obat' => $nama_obat,
			'jenis_obat' => $jenis_obat,
			'satuan' => $satuan,
			'stok_obat' => $stok_obat,
			'harga_beli' => $harga_beli,
			'harga_jual' => $harga_jual
		);

		$where = array(
			'id_obat' => $id_obat
		);

		$this->M_Crud_Gudang->update_data_obat($where,$data,'obat');
		redirect('gudang_admin/c_crud_gudang/tampil_obat');
	}

	public function edit_supplier($id_supplier){
		$where = array('id_supplier' => $id_supplier);
		$data['username'] = $this->session->userdata('username');
		$data['supplier'] = $this->M_Crud_Gudang->edit_data_supplier($where,'supplier')->result();
		$this->load->view('gudang_admin/update_supplier',$data);
	}

	public function update_supplier(){
			$id_supplier = $this->input->post('id_supplier');
			$nama_supplier = $this->input->post('nama_supplier');
			$alamat = $this->input->post('alamat');
			$kontak = $this->input->post('kontak');
			$barang_masuk = $this->input->post('barang_masuk');
			$harga_beli = $this->input->post('harga_beli');

			$data = array(
				'nama_supplier' => $nama_supplier,
				'alamat' => $alamat,
				'kontak' => $kontak,
				'barang_masuk' => $barang_masuk,
				'harga_beli' => $harga_beli
				);

		$where = array(
			'id_supplier' => $id_supplier
		);

		$this->M_Crud_Gudang->update_data_supplier($where,$data,'supplier');
		redirect('gudang_admin/c_crud_gudang/tampil_suplier');
	}

	public function search_obat(){
			$keyword = $this->input->post('keyword');
			$data['username'] = $this->session->userdata('username');
			$data['obat']=$this->M_Crud_Gudang->get_product_obat($keyword);
			$this->load->view('gudang_admin/gudang_obat',$data);
	}

	public function search_supplier(){
			$keyword = $this->input->post('keyword');
			$data['username'] = $this->session->userdata('username');
			$data['supplier']=$this->M_Crud_Gudang->get_product_supplier($keyword);
			$this->load->view('gudang_admin/gudang_supplier',$data);
	}

	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('auth');
	}
}
?>
