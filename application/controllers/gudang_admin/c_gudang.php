<?php
class C_gudang extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="") {
			redirect('auth');
		}
		$this->load->helper('text');
		$this->load->model('gudang/M_Crud_Gudang');
	}
	public function index() {
		$data['username'] = $this->session->userdata('username');
		$this->load->view('gudang_admin/index', $data);
	}

	public function register_obat() {
		$data['username'] = $this->session->userdata('username');
		$this->load->view('gudang_admin/register_obat', $data);
	}

	public function register_suplier() {
		$data['username'] = $this->session->userdata('username');
		$this->load->view('gudang_admin/register_suplier', $data);
	}
	
	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('auth');
	}
}
?>
