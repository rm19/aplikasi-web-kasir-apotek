<?php
class C_manager extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="") {
			redirect('auth');
		}
		$this->load->helper('text');
		$this->load->model('manager/M_Crud_Manager');
	}
	public function form_register() {//form register karyawan
		$data['username'] = $this->session->userdata('username');
		$this->load->view('Manager/register_karyawan', $data);
	}

	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('auth');
	}
}
?>
