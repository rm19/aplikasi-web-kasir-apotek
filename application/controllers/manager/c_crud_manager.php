<?php
class C_crud_manager extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="") {
			redirect('auth');
		}
		$this->load->helper('text');
		$this->load->model('manager/M_Crud_Manager');
	}

	public function index() {//tampil_karyawan
		$data['username'] = $this->session->userdata('username');
		$data['user'] = $this->M_Crud_Manager->tampil_user()->result();
		$this->load->view('manager/karyawan', $data);
	}

	public function hapus_karyawan($uid){
		$where = array('uid' => $uid);
		$this->M_Crud_Manager->hapus_user($where,'user');
		redirect('manager/c_crud_manager/index');
	}

	public function tambah_karyawan(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$level = $this->input->post('level');

		$data = array(
			'username' => $username,
			'password' => $password,
			'level' => $level
			);

		$this->M_Crud_Manager->input_user($data,'user');
		redirect('manager/c_crud_manager/index');
	}

	public function edit_karyawan($uid){
		$where = array('uid' => $uid);
		$data['username'] = $this->session->userdata('username');
		$data['user'] = $this->M_Crud_Manager->edit_user($where,'user')->result();
		$this->load->view('Manager/update_karyawan',$data);
	}

	public function update_karyawan(){
		$uid = $this->input->post('uid');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$level = $this->input->post('level');

		$data = array(
			'username' => $username,
			'password' => $password,
			'level' => $level
			);

		$where = array(
			'uid' => $uid
		);

		$this->M_Crud_Manager->update_user($where,$data,'user');
		redirect('manager/c_crud_manager/index');
	}

	public function search(){
			$keyword = $this->input->post('keyword');
			$data['username'] = $this->session->userdata('username');
			$data['user']=$this->M_Crud_Manager->get_product_keyword($keyword);
			$this->load->view('Manager/karyawan',$data);
	}

	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('auth');
	}
}
?>
