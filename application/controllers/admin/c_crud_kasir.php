<?php
class C_crud_kasir extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="") {
			redirect('auth');
		}
		$this->load->helper('text');
		$this->load->model('admin/M_Crud');
	}

	public function tampil_transaksi() {
		$data['username'] = $this->session->userdata('username');
		$data['transaksi'] = $this->M_Crud->lihat_transaksi()->result();
		$this->load->view('admin/riwayat_transaksi', $data);
	}

	public function tampil_obat() {
		$data['username'] = $this->session->userdata('username');
		$data['obat'] = $this->M_Crud->tampil_data()->result();
		$this->load->view('admin/tampil_obat', $data);
	}

	public function tampil_supplier() {
		$data['username'] = $this->session->userdata('username');
		$data['supplier'] = $this->M_Crud->tampil_data_supplier()->result();
		$this->load->view('admin/tampil_supplier', $data);
	}

	public function search_obat(){
			$keyword = $this->input->post('keyword');
			$data['username'] = $this->session->userdata('username');
			$data['obat']=$this->M_Crud->get_product_obat($keyword);
			$this->load->view('admin/tampil_obat',$data);
	}

	public function hapus_transaksi($id_transaksi){
		$where = array('kode_transaksi' => $id_transaksi);
		$this->M_Crud->hapus_transaksi($where,'transaksi');
		redirect('admin/c_crud_kasir/tampil_transaksi');
	}

	// public function edit_transaksi($id_transaksi){
	// 	$where = array('kode_transaksi' => $id_transaksi);
	// 	$data['username'] = $this->session->userdata('username');
	// 	$data['transaksi'] = $this->M_Crud->edit_data_transaksi($where,'transaksi')->result();
	// 	$this->load->view('admin/update_transaksi',$data);
	// }
	//
	// public function update_transaksi(){
	// 	$id_transaksi = $this->input->post('kode_transaksi');
	// 	$tanggal = $this->input->post('tgl');
	// 	$kode_obat = $this->input->post('kode_obat');
	// 	$nama_obat = $this->input->post('nama_obat');
	// 	$total_penjualan = $this->input->post('harga_jual');
	// 	$total_barang = $this->input->post('qty');
	//
	// 	$data = array(
	// 		'tanggal' => $tanggal,
	// 		'kode_obat' => $kode_obat,
	// 		'nama_obat' => $nama_obat,
	// 		'total_penjualan' => $total_penjualan,
	// 		'total_barang' => $total_barang
	// 		);
	//
	// 	$where = array(
	// 		'kode_transaksi' => $id_transaksi
	// 	);
	//
	// 	$this->M_Crud->update_data_transaksi($where,$data,'transaksi');
	// 	redirect('admin/c_crud_kasir/tampil_transaksi');
	// }

	// public function search_obat_transaksi(){
	// 		$keyword = $this->input->post('keyword');
	// 		$data['username'] = $this->session->userdata('username');
	// 		$data['obat']=$this->M_Crud->get_product_obat_transaksi($keyword);
	// 		$this->load->view('admin/index',$data);
	// }

	public function search_supplier(){
			$keyword = $this->input->post('keyword');
			$data['username'] = $this->session->userdata('username');
			$data['supplier']=$this->M_Crud->get_product_supplier($keyword);
			$this->load->view('admin/tampil_supplier',$data);
	}

	public function tambah_transaksi(){
		$tanggal = $this->input->post('tgl');
		$kode_obat = $this->input->post('kode_obat');
		$nama_obat = $this->input->post('nama_obat');
		$total_penjualan = $this->input->post('harga_jual');
		$total_barang = $this->input->post('qty');

		$data = array(
			'tanggal' => $tanggal,
			'kode_obat' => $kode_obat,
			'nama_obat' => $nama_obat,
			'total_penjualan' => $total_penjualan,
			'total_barang' => $total_barang
			);

		$this->M_Crud->input_transaksi($data,'transaksi');
		redirect('admin/c_admin/index');
	}

	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('auth');
	}
}
?>
